const express = require('express');
const app = express();
// let lineupController = require('./controller/lineupController.js');
let bodyParser = require('body-parser');
let cors = require('cors');
let path = require('path');
let Error =  require('./models/structures/Err.js');
let useragent = require('express-useragent');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test2');

//TODO! Delete cors and config the server.
app.use(cors());
//Add to req the device the user uses.
app.use(useragent.express());
app.use(bodyParser.json());
//Catch json format is bad. this will fire at every request!
// app.use(function(err, req, res, next) {
//     if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
//         res.status(400).json(new Error("Bad json format.", 400));
//     } else if (req.body === undefined){
//         res.status(400).json("No data received.");
//     }
// });



// app.use('/lineup', lineupController);


//Generic get
// app.get('/', (request, response) => {
//   response.json({
//     chance: "bbb"
//   })
// });

app.listen(30007);
console.log("Server is running");
